
#include <Adafruit_GPS.h>
#include <SPI.h>
#include <SD.h>
#include <RTCZero.h>
#include "LowPower.h"
#include <Wire.h>












 int dia, mes, anio, hora;

float vBateria;




#define GPSSerial Serial1
#define CUENTA_MAXIMA 5
#define VBATPIN A7
// Conectar el GPS por el puerto serie Hardware
Adafruit_GPS GPS(&GPSSerial);
  
/*Varialbles*/

RTCZero rtc;
bool flag = false;
/* Change these values to set the current initial time */
const byte seconds = 0;
const byte minutes = 00;
const byte hours = 15;
byte hora_alarm = 15;
byte minuto_alarm = 0;
byte segundo_alarm = 0;
byte dato_alarm = 0;

/* Change these values to set the current initial date */
const byte day = 13;
const byte month = 8;
const byte year = 20;

char c ;
int estado = 100,  cuenta = 1, lectura = 1;
int i, analogPin, sensor,columna;
uint32_t timer = millis();


 int hr = 0;
int mn = 0;
int timezonehr = 3; //Timezone hour offset
int timezonemn = 0;  //Timezone minute offsetunsigned long  tiempo = 0;
int grado, minuto, segundo, grado_long, minuto_long, segundo_long, dec_segundo_long, grado_lat, minuto_lat, segundo_lat, dec_segundo_lat;

// Set GPSECHO to 'false' to turn off echoing the GPS data to the Serial console
// Set to 'true' if you want to debug and listen to the raw GPS sentences
#define GPSECHO false

#define INTERVALO 1000
/*Constantes*/
#define MENU                     100
#define DIAGNOSTICO              200
#define DORMIDO                  300
#define LEER_SENSORES            400 


// Initialize DHT sensor.
// Note that older versions of this library took an optional third parameter to
// tweak the timings for faster processors.  This parameter is no longer needed
// as the current DHT reading algorithm adjusts itself to work on faster procs.





void setup() {
   Serial.begin(9600);
    while (!Serial);
 
   /*Salida Digital Pin 13*/
  pinMode(LED_BUILTIN, OUTPUT);

  /*Entrada Digital Pin 11 - Modo PULLUP - Por Interrupción 
  Desactiva el WhatDog para la Transmisión de Datos*/
  //pinMode(11, INPUT_PULLUP);
 // digitalWrite(11,HIGH);


  
 
  /*La velocidad para el protocolo NMEA es de 9600 baudios predeterminada para los GPS MTK de Adafruit; algunos usan 4800*/
  // 9600 NMEA is the default baud rate for Adafruit MTK GPS's- some use 4800
  GPS.begin(9600);
  // uncomment this line to turn on RMC (recommended minimum) and GGA (fix data) including altitude
  GPS.sendCommand(PMTK_SET_NMEA_OUTPUT_RMCGGA);
  // uncomment this line to turn on only the "minimum recommended" data
  //GPS.sendCommand(PMTK_SET_NMEA_OUTPUT_RMCONLY);
  // For parsing data, we don't suggest using anything but either RMC only or RMC+GGA since
  // the parser doesn't care about other sentences at this time
  // Set the update rate
  GPS.sendCommand(PMTK_SET_NMEA_UPDATE_1HZ); // 1 Hz update rate
  // For the parsing code to work nicely and have time to sort thru the data, and
  // print it out we don't suggest using anything higher than 1 Hz
     
  // Request updates on antenna status, comment out to keep quiet
  GPS.sendCommand(PGCMD_ANTENNA);

  delay(1000);
  
  // Ask for firmware version
  GPSSerial.println(PMTK_Q_RELEASE);

 
  rtc.begin();

  rtc.setTime(hours, minutes, seconds);
  
 
  rtc.setDate(day, month, year);

 

}

void loop() {
 
if(Serial.available() > 0){ //Comprobar si recibe un byte via Serial

   estado = Serial.parseInt();
  
   if ((estado >=1)&&(estado <=60)){ 
   
      dato_alarm = (byte) estado;
      //Serial.print(" Tiempo de Alarma: ");
      Serial.print(dato_alarm);
      Serial.println(" MINUTOS ");
      minuto_alarm = dato_alarm;
    } 
 
  } 
 
  switch(estado){
    
     case MENU: 
                        digitalWrite(LED_BUILTIN, LOW);
                        while (!Serial) {
                            ; // wait for serial port to connect. Needed for native USB port only
                         }
                        Serial.println(" REALIZE TEST DE DIAGNOSTICO");
                        Serial.println( "200 - NODO / DIAGNOSTICO ");
                        Serial.println( "300 - NODO / BAJO CONSUMO ");
                       
                       
                                             
                        
                        estado = 0;
                       
                         break;
   case DIAGNOSTICO:
                              /*Desactivar Lora Chip Select 8, pasa utilizar SD Card*/
                              // pinMode(8,OUTPUT);
                              // digitalWrite(8,HIGH);
                              vBateria = analogRead(VBATPIN); 
                              vBateria *= 2;    // we divided by 2, so multiply back 
                              vBateria *= 3.3;  // Multiply by 3.3V, our reference voltage
                              vBateria /= 1024; // convert to voltage 
                               Serial.print("Bateria : ");
                               Serial.println(vBateria);
                           
                                 Serial.println("Tarjeta SD Inicializada.");
                                 Serial.println(" INGRESE EL TIEMPO ENTRE MUESTRAS EN MINUTOS ");                 
                                estado = MENU;
                               
                        
                            break;
    
    case DORMIDO:


                                   if (flag){
                                    rtc.setTime(15, 0, 0);
  
                                     rtc.setDate(day, month, year);
                                    
                                      flag = false;
                                   }
                                 
                                  digitalWrite(11,HIGH);
                                  digitalWrite(10,HIGH);
                                  digitalWrite(6,HIGH);
                                  digitalWrite(LED_BUILTIN, LOW);
                                  GPS.standby();
                                  rtc.setAlarmTime(hora_alarm, minuto_alarm, segundo_alarm);
                                  
                                  rtc.enableAlarm(rtc.MATCH_HHMMSS);

                                  rtc.attachInterrupt(alarmSensado);
                                  
                                 // rtc.standbyMode(); 
                                  LowPower.standby();
                                  
                                  //estado = 0;
                                  break;
   

    
    

    
    case LEER_SENSORES:
                 
                        GPS.wakeup();
                        
                       c = GPS.read();
                                       
                       if (GPSECHO)
                       if (c) Serial.print(c);
  
                           // if a sentence is received, we can check the checksum, parse it...
                       if (GPS.newNMEAreceived()) {
                       // a tricky thing here is if we print the NMEA sentence, or data
                       // we end up not listening and catching other sentences!
                       // so be very wary if using OUTPUT_ALLDATA and trytng to print out data
                      //Serial.println(GPS.lastNMEA()); // this also sets the newNMEAreceived() flag to false
                      if (!GPS.parse(GPS.lastNMEA())) // this also sets the newNMEAreceived() flag to false
                      return; // we can fail to parse a sentence in which case we should just wait for another
                        }
                       // if millis() or timer wraps around, we'll just reset it
                      if (timer > millis()) timer = millis();
                                 
                      // approximately every 2 seconds or so, print out the current stats
                       if (millis() - timer > 2000) {
                       timer = millis(); // reset the timer
                       hr = GPS.hour;
                                        
                       hr = hr - timezonehr;
                       if (hr > 23) {
                         hr = hr - 24;
                       }
                       else {
                        if (hr < 0) {
                              hr = hr + 24;
                         }
                       }
                  if (hr > 12) {
                         hr = hr - 12;
                  }
                    else {
                                        
                     }
                   if (hr == 0) {
                     hr = 12;
                   }
            Serial.print(hr); Serial.print(':');
            Serial.print(GPS.minute, DEC); Serial.print(':');
            Serial.print(GPS.seconds, DEC); Serial.print('.');
            Serial.println(GPS.milliseconds);
            Serial.print("Día: ");
            Serial.print(GPS.day, DEC); Serial.print('/');
            Serial.print(GPS.month, DEC); Serial.print("/20");
            Serial.println(GPS.year, DEC);

            if (GPS.fix) {
             Serial.print("Localizacion: ");
             Serial.print(GPS.latitude, 4); Serial.print(GPS.lat);
             Serial.print(", ");
             Serial.print(GPS.longitude, 4); Serial.println(GPS.lon);
             Serial.print("Speed (knots): "); Serial.println(GPS.speed);
             Serial.print("Angulo: "); Serial.println(GPS.angle);
             Serial.print("Altitud: "); Serial.println(GPS.altitude);
             Serial.print("Satelites: "); Serial.println((int)GPS.satellites);
             }
           
            
                 
                // if  (cuenta < CUENTA_MAXIMA){
                 if  (cuenta < 10){
                     cuenta ++;
                     estado = LEER_SENSORES;
                      Serial.println(cuenta);
                  }
               else {
                  cuenta = 1;
              
                 
                  estado = DORMIDO;        
               }            
                  }
                   
                  break;

   
        
   
    
     
     
     

    
    //default:
               //  break;*/
                          
  }
}


void alarmSensado()
{ 
   flag = true;
  Serial.println(" INTERRUPCION ");
   estado = LEER_SENSORES;
           
 }
